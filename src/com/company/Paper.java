package com.company;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Paper {
    JFrame jFrame = new JFrame("Paint");
    JPanel jPanel = new JPanel(), rightPanel = new JPanel(null);
    JButton buttonRed, buttonBlue, buttonBlack, buttonViolet, buttonColorChooser, buttonDelete, buttonRubber;
    JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    private Graphics2D g;
    private final int FPS_MIN = 0, FPS_MAX = 100, FPS_INIT = 3;
    private int currentX, currentY, x2, y2, valueSlider;
    private Color colors;
    JSlider slider = new JSlider(JSlider.VERTICAL, FPS_MIN, FPS_MAX, FPS_INIT);

    public Paper() {
        initialFrame();
        splitPane.updateUI();
    }

    public void initialFrame() {
        jFrame.setVisible(true);
        jFrame.setResizable(false);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        jFrame.setBounds(dimension.width / 2 - 200, dimension.height / 2 - 200, 560, 520);
        jFrame.add(jPanel);
        jPanel.setLayout(null);
        jFrame.setDefaultCloseOperation(jFrame.EXIT_ON_CLOSE);
        jFrame.add(splitPane);
        splitPane.setDividerLocation(420);
        splitPane.setRightComponent(rightPanel);
        splitPane.setLeftComponent(jPanel);
        jPanel.setBackground(Color.WHITE);
        initialElement();
        canvas();
        palette();
        logicButton();
        logicSlider();
    }

    public void initialElement() {
        buttonRed = new JButton();
        buttonRed.setBackground(new Color(255, 26, 26));
        buttonRed.setBounds(10, 10, 50, 50);

        buttonBlue = new JButton();
        buttonBlue.setBackground(new Color(0, 30, 202));
        buttonBlue.setBounds(70, 10, 50, 50);

        buttonBlack = new JButton();
        buttonBlack.setBackground(new Color(14, 13, 13));
        buttonBlack.setBounds(10, 70, 50, 50);

        buttonViolet = new JButton();
        buttonViolet.setBackground(new Color(152, 26, 255));
        buttonViolet.setBounds(70, 70, 50, 50);

        buttonColorChooser = new JButton("Select");
        buttonColorChooser.setBounds(10, 140, 110, 110);

        buttonDelete = new JButton("Clear");
        buttonDelete.setBounds(10, 260, 110, 30);

        buttonRubber = new JButton("Rubber");
        buttonRubber.setBounds(10, 300, 110, 30);

        slider.setBounds(40, 350, 50, 100);
        slider.setMajorTickSpacing(25);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
    }

    public void canvas() {
        jPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();

                g = (Graphics2D) jPanel.getGraphics();

                x2 = currentX;
                y2 = currentY;
                g.setColor(colors);
                g.setStroke(new BasicStroke(valueSlider));
            }
        });

        jPanel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();

                g.drawLine(currentX, currentY, x2, y2);
                g.setColor(colors);
                g.setStroke(new BasicStroke(valueSlider));

                x2 = currentX;
                y2 = currentY;
            }
        });
    }

    public void palette() {
        rightPanel.add(buttonRed);
        rightPanel.add(buttonBlue);
        rightPanel.add(buttonBlack);
        rightPanel.add(buttonViolet);
        rightPanel.add(buttonColorChooser);
        rightPanel.add(buttonDelete);
        rightPanel.add(buttonRubber);
        rightPanel.add(slider);
    }

    public void logicButton() {
        buttonRed.addActionListener(actionEvent -> colors = (new Color(255, 26, 26)));
        buttonBlue.addActionListener(actionEvent -> colors = (new Color(0, 30, 202)));
        buttonBlack.addActionListener(actionEvent -> colors = (new Color(14, 13, 13)));
        buttonViolet.addActionListener(actionEvent -> colors = (new Color(152, 26, 255)));
        buttonColorChooser.addActionListener(actionEvent -> colors = JColorChooser
                .showDialog(null, "Select a color", Color.BLACK));
        buttonDelete.addActionListener(actionEvent -> jPanel.updateUI());
        buttonRubber.addActionListener(actionEvent -> colors = (new Color(255, 255, 255)));
    }

    public void logicSlider() {
        slider.addChangeListener((ChangeListener) e -> {
            JSlider source = (JSlider) e.getSource();
            if (!source.getValueIsAdjusting()) {
                valueSlider = source.getValue();
            }
        });
    }
}

